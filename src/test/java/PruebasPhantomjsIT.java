import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;


import java.util.ArrayList;

public class PruebasPhantomjsIT{
private static WebDriver driver=null;

    @Test
    public void tituloIndexTest() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
                new String[] { "--web-security=no", "--ignore-ssl-errors=yes" });
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        assertEquals("Votacion mejor jugador liga ACB", driver.getTitle(), "El titulo no es correcto");
        System.out.println(driver.getTitle());
        driver.close();
        driver.quit();
    }

    @Test
    public void votosACeroTest() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
                new String[] { "--web-security=no", "--ignore-ssl-errors=yes" });
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");

         //Pulsación del botón poner votos a 0
         driver.findElement(By.id("resetVotes")).click();
         driver.findElement(By.id("backToIndex")).click();
         //Pulsación del botón ver votos
         driver.findElement(By.id("verVotos")).click();
        WebElement tableVotes = driver.findElement(By.id("tablaVotos")); //recuperamos tabla de votos
        ArrayList<WebElement> listaFilas = new ArrayList<>(tableVotes.findElements(By.tagName("tr")));
        listaFilas.remove(0);
        ArrayList<WebElement> listaVotos = new ArrayList<>();
        //Se obtiene la lista de votos de cada jugador de cada fila
        for(int i = 0; i < listaFilas.size(); i++)
        {
            ArrayList<WebElement> celdas = (ArrayList<WebElement>) listaFilas.get(i).findElements(By.tagName("td"));
            listaVotos.add(celdas.get(1));
        }
        int expResult = listaVotos.size();
        int numReseteados = 0;
        //Se comprueba que el valor de los votos de cada jugador están a cero
        for( int j = 0; j < listaVotos.size(); j++)
        {
            if(listaVotos.get(j).getText().trim().equals("0"))
            {
               numReseteados++; 
            }
        }

        assertEquals(expResult, numReseteados, "No se han reseteado los votos correctamente");

    }

    @Test
    void votaOtro()
    {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new
        String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");

        driver.findElement(By.id("rbOtros")).click();
        String nombreOtro = "Doncic";
        driver.findElement(By.id("txtOtros")).sendKeys(nombreOtro);
        driver.findElement(By.id("votar")).click();
        driver.findElement(By.id("backToIndex")).click();
        driver.findElement(By.id("verVotos")).click();

        
        WebElement tableVotes = driver.findElement(By.id("tablaVotos")); //recuperamos tabla de votos
        ArrayList<WebElement> listaFilas = new ArrayList<>(tableVotes.findElements(By.tagName("tr")));
        listaFilas.remove(0);
       


        int expResult = 1;
        int votosOtro = 0;
        //Se comprueba que el valor de los votos de cada jugador están a cero
        for( int i = 0; i < listaFilas.size(); i++)
        {
            ArrayList<WebElement> celdas = (ArrayList<WebElement>) listaFilas.get(i).findElements(By.tagName("td"));
            if(celdas.get(0).getText().trim().equals(nombreOtro))
            {
                votosOtro = Integer.parseInt(celdas.get(1).getText().trim()); 
            }
        }

        assertEquals(expResult, votosOtro, "El jugador no ha recibido el voto");
        

    }

}
