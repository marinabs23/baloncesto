<%@ page import="java.util.ArrayList"%>

<!DOCTYPE >
<html lang="es">
  <head>
    <title>Votación mejor jugador liga ACB</title>
    <link href="estilos.css" rel="stylesheet" type="text/css" />
    <link
      rel="stylesheet"
      href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
      crossorigin="anonymous"
    />
  </head>
  <body class="resultado">
    <p style="font-size: 40">Votaci&oacute;n al mejor jugador de la liga ACB</p>
      <hr />
      <h2>Resultados</h2>
      <table class="table" id="tablaVotos">
         <th><b>Jugador</b></th>
         <th><b>N&uacute;mero de votos</b></th>

       <% ArrayList<String[]> listaVotos = (ArrayList<String[]>) session.getAttribute("listaVotos");
          String[] jugador;
          for(int i =0; i<listaVotos.size() ; i++){
            jugador = listaVotos.get(i);
         %>
           <tr> 
               <td><%=jugador[0]%></td>
               <td><%=jugador[1]%></td>
           </tr>
           <%}%>
       </table>

      <br />
    
    <a id="backToIndex" href="index.html"> Ir al comienzo</a>
  </body>
</html>
