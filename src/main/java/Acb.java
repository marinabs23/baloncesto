
import java.io.*;
import java.util.ArrayList;

import javax.servlet.*;
import javax.servlet.http.*;

public class Acb extends HttpServlet {

    private ModeloDatos bd;

    @Override
    public void init(ServletConfig cfg) throws ServletException {
        bd = new ModeloDatos();
        bd.abrirConexion();
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        HttpSession s = req.getSession(true);
        String action = req.getParameter("action"); // nombre del servlet

        if (action.equalsIgnoreCase("votar")) { //acción de votar
            String nombreP = req.getParameter("txtNombre");
            String nombre = req.getParameter("R1");

            if (nombre.equals("Otros")) {
                nombre = (String) req.getParameter("txtOtros");
            }
            if (bd.existeJugador(nombre)) {
                bd.actualizarJugador(nombre);
            } else {
                bd.insertarJugador(nombre);
            }
            s.setAttribute("nombreCliente", nombreP);
            // Llamada a la página jsp que nos da las gracias
            res.sendRedirect(res.encodeRedirectURL("TablaVotos.jsp"));
        }

        if (action.equalsIgnoreCase("resetVotes")) { //accion de resetear votos
            bd.resetearVotos();
            res.sendRedirect(res.encodeRedirectURL("votosReset.jsp"));
           
        }
        if (action.equalsIgnoreCase("verVotos")) { // votos
            ArrayList<String[]> listaVotos;
            listaVotos = bd.getVotos();
            s.setAttribute("listaVotos", listaVotos);
            res.sendRedirect(res.encodeRedirectURL("verVotos.jsp"));
           
        }
    }

    @Override
    public void destroy() {
        bd.cerrarConexion();
        super.destroy();
    }
}
