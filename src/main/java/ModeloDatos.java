import java.sql.*;
import java.util.ArrayList;

public class ModeloDatos {

    private Connection con;
    private Statement set;
    private ResultSet rs;

    public void abrirConexion() {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            // Con variables de entorno
            String dbHost = System.getenv().get("DATABASE_HOST");
            String dbPort = System.getenv().get("DATABASE_PORT");
            String dbName = System.getenv().get("DATABASE_NAME");
            String dbUser = System.getenv().get("DATABASE_USER");
            String dbPass = System.getenv().get("DATABASE_PASS");

            String url = dbHost + ":" + dbPort + "/" + dbName;
            con = DriverManager.getConnection(url, dbUser, dbPass);

        } catch (Exception e) {
            // No se ha conectado
            System.out.println("No se ha podido conectar. El error es: " + e.getMessage());
        }
    }

    public boolean existeJugador(String nombre) {
        boolean existe = false;
        String cad;
        try {
            abrirConexion();
            set = con.createStatement();
            rs = set.executeQuery("SELECT * FROM Jugadores");
            while (rs.next()) {
                cad = rs.getString("Nombre");
                cad = cad.trim();
                if (cad.compareTo(nombre.trim()) == 0) {
                    existe = true;
                }
            }
            rs.close();
            set.close();
        } catch (Exception e) {
            // No lee de la tabla
            System.out.println("No lee de la tabla, El error es: " + e.getMessage());
        }
        return (existe);
    }

    public void actualizarJugador(String nombre) {
        try {
            abrirConexion();
            set = con.createStatement();
            set.executeUpdate("UPDATE Jugadores SET votos=votos+1 WHERE nombre " + " LIKE '%" + nombre + "%'");
            rs.close();
            set.close();
        } catch (Exception e) {
            // No modifica la tabla
            System.out.println("No modifica la tabla. El error es: " + e.getMessage());
        }
    }


    public void insertarJugador(String nombre) {
        try {
            abrirConexion();
            set = con.createStatement();
            set.executeUpdate("INSERT INTO Jugadores " + " (nombre,votos) VALUES ('" + nombre + "',1)");
            rs.close();
            set.close();
        } catch (Exception e) {
            // No inserta en la tabla
            System.out.println("No inserta en la tabla, el error es: "  + e.getMessage());
        }
    }

    public void resetearVotos() {
        try {
            abrirConexion();
            set = con.createStatement();
            set.executeUpdate("UPDATE Jugadores SET votos=0");
            rs.close();
            set.close();
        } catch (Exception e) {
            // No modifica la tabla
            System.out.println("Poner votos a 0 - no modifica la tabla, el error es: "  + e.getMessage());
        }
    }

    public int getVotosJugador(String nombre) {
        int numVotos=0;

        try {
            abrirConexion();
            set = con.createStatement();
            rs = set.executeQuery("SELECT * FROM Jugadores WHERE nombre='" + nombre + "'");
            if (rs.next()) {
                numVotos = rs.getInt("votos");
            }
            rs.close();
            set.close();

        } catch (Exception e) {
            System.out.println("No recupera los votos de un jugador, el error es: "  + e.getMessage());
        }
        return (numVotos);
    }

    public ArrayList<String[]> getVotos() {
        ArrayList<String[]> listVotos = new ArrayList<String[]>();
        abrirConexion();
        try {
           
            set = con.createStatement();
            rs = set.executeQuery("SELECT * FROM Jugadores");
            while (rs.next()) {
                String[] pareja = {rs.getString("Nombre"),rs.getString("Votos")};
                listVotos.add(pareja);
            }
            rs.close();
            set.close();
        } catch (Exception e) {
            // No lee de la tabla
            System.out.println("No lee votos de la tabla, el error es: "  + e.getMessage());
        }
        return listVotos;
    }

    public void cerrarConexion() {
        try {
            con.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
